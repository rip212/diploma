package com.rip21.diploma.dao;

import com.rip21.diploma.entity.Account;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by puha2_000 on 06.05.14.
 */
public interface AccountDao extends CrudRepository<Account, Long> {

    //    @Query("select o from Accounts where user_id = ?1")
    public List<Account> findByUser_Id(long userid);
}
