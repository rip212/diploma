package com.rip21.diploma.dao;

import com.rip21.diploma.entity.History;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by puha2_000 on 06.05.14.
 */
public interface HistoryDao extends CrudRepository<History, Long> {
    public List<History> findByAccount_Id(long id);
}
