package com.rip21.diploma.dao;

import com.rip21.diploma.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserDao extends CrudRepository<User, Long> {

    User findByLogin(String login);

    User findByEmail(String email);

    User findByValidationCode(int validationCode);
}
