package com.rip21.diploma.service;

import com.rip21.diploma.dao.HistoryDao;
import com.rip21.diploma.entity.Account;
import com.rip21.diploma.entity.History;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by puha2_000 on 24.05.14.
 */
@Service
public class HistoryService {

    public static final String DEPOSIT = "Deposit";
    public static final String WITHDRAW = "Withdraw";
    public static final String TRANSFER = "Transfer";

    @Autowired
    HistoryDao historyDao;

    public List<History> findHistoryByAccountId(long id) {
        return historyDao.findByAccount_Id(id);
    }

    public void addHistory(Account activeAccount, BigDecimal amount, String type) {
        History historyRecord = new History(
                activeAccount,
                type,
                amount.toString(),
                activeAccount.getBalance().toString(),
                activeAccount.getOverdraft().toString(),
                new Date()
        );

        historyDao.save(historyRecord);
    }

    public void deleteAllHistory(Account activeAccount) {
        historyDao.delete(activeAccount.getHistory());
    }
}
