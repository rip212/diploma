package com.rip21.diploma.service;

import com.google.common.collect.Lists;
import com.rip21.diploma.RoleConstants;
import com.rip21.diploma.dao.UserDao;
import com.rip21.diploma.entity.User;
import com.rip21.diploma.service.helper.RegistrationData;
import com.rip21.diploma.service.helper.SendMailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by puha2_000 on 14.05.14.
 */
@Service
public class UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private SendMailService sendMailService;

    public void saveUser(User b) {
        userDao.save(b);
    }

    public void registerUser(RegistrationData registrationData) {
        User user = new User();
        user.setLogin(registrationData.getLogin());
        user.setPassword(registrationData.getPassword());
        user.setEmail(registrationData.getEmail());
        user.setName(registrationData.getName());
        user.setRole(RoleConstants.ROLE_INACTIVE_USER);
        user.setValidationCode(user.hashCode());

        userDao.save(user);

        sendMailService.sendConfirmEmail(user);
    }

    public User getUserById(long id) {
        return userDao.findOne(id);
    }

    public User getUserByLogin(final String login) {
        return userDao.findByLogin(login);
    }

    public User getUserByEmail(final String email) {
        return userDao.findByEmail(email);
    }


    public User getUserByValidationCode(int validationCode) {
        return userDao.findByValidationCode(validationCode);
    }

    public void deleteUserById(long id) {
        userDao.delete(id);
    }

    public List<User> getAllUsers() {
        return Lists.newArrayList(userDao.findAll());
    }
}
