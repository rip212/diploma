package com.rip21.diploma.service.helper;

import com.rip21.diploma.RoleConstants;
import org.springframework.security.authentication.AuthenticationTrustResolverImpl;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.expression.WebSecurityExpressionRoot;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by puha2_000 on 14.05.14.
 */
@Service
@Deprecated
public class SecurityInterceptor extends HandlerInterceptorAdapter {
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        if (modelAndView != null) {
            ServletRequest req = (ServletRequest) request;
            ServletResponse resp = (ServletResponse) response;
            FilterInvocation filterInvocation = new FilterInvocation(req, resp, new FilterChain() {
                public void doFilter(ServletRequest request, ServletResponse response) throws IOException, ServletException {
                    throw new UnsupportedOperationException();
                }
            });


            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (authentication != null) {
                WebSecurityExpressionRoot sec = new WebSecurityExpressionRoot(authentication, filterInvocation);
                sec.setTrustResolver(new AuthenticationTrustResolverImpl());
                modelAndView.getModel().put("sec", sec);
            } else {
                // Dirty hack for tests
                // IF SOMEONE CAN EXPLAIN THIS GARBAGE, I NEED IT
                class Sec {
                    UserDetails principal = new UserDetails() {
                        @Override
                        public Collection<? extends GrantedAuthority> getAuthorities() {
                            return Collections.singleton(new SimpleGrantedAuthority(RoleConstants.ROLE_ANONYMOUS));
                        }

                        @Override
                        public String getPassword() {
                            return "";
                        }

                        @Override
                        public String getUsername() {
                            return "ANONYMOUS";
                        }

                        @Override
                        public boolean isAccountNonExpired() {
                            return true;
                        }

                        @Override
                        public boolean isAccountNonLocked() {
                            return true;
                        }

                        @Override
                        public boolean isCredentialsNonExpired() {
                            return true;
                        }

                        @Override
                        public boolean isEnabled() {
                            return true;
                        }
                    };

                    public UserDetails getPrincipal() {
                        return principal;
                    }
                }

                modelAndView.getModel().put("sec", new Sec());
            }
        }
    }
}