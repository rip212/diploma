package com.rip21.diploma.service.helper;

import com.rip21.diploma.entity.History;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.util.List;

/**
 * Created by puha2_000 on 01.06.14.
 */
public class ExcelGenerator {

    public static XSSFWorkbook getWorkbook(List<History> historyList) {

        XSSFWorkbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet();
        Row firstRow = sheet.createRow(0);
        firstRow.createCell(0).setCellValue("Transaction ID");
        firstRow.createCell(1).setCellValue("Operation type");
        firstRow.createCell(2).setCellValue("Amount");
        firstRow.createCell(3).setCellValue("Balance");
        firstRow.createCell(4).setCellValue("Overdraft");
        firstRow.createCell(5).setCellValue("Date");
        firstRow.getCell(0).setCellStyle(CellStyles.greenBackground(workbook));
        firstRow.getCell(1).setCellStyle(CellStyles.greenBackground(workbook));
        firstRow.getCell(2).setCellStyle(CellStyles.greenBackground(workbook));
        firstRow.getCell(3).setCellStyle(CellStyles.greenBackground(workbook));
        firstRow.getCell(4).setCellStyle(CellStyles.greenBackground(workbook));
        firstRow.getCell(5).setCellStyle(CellStyles.greenBackground(workbook));

        int rowNum = 1;
        for (History history : historyList) {
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(history.getId());
            row.createCell(1).setCellValue(history.getType());
            row.createCell(2).setCellValue(history.getAmount());
            row.createCell(3).setCellValue(history.getBalance());
            row.createCell(4).setCellValue(history.getOverdraft());
            row.createCell(5).setCellValue(history.getDate());
            row.getCell(5).setCellStyle(CellStyles.date(workbook));
        }
        for (Row row : sheet) {
            for (int colNum = 0; colNum < row.getLastCellNum(); colNum++)
                workbook.getSheetAt(0).autoSizeColumn(colNum);
        }
        return workbook;
    }

}
