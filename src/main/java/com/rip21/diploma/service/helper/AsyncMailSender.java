package com.rip21.diploma.service.helper;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;


public class AsyncMailSender implements MailSender {


    @Autowired
    private MailSender mailSender;

    @Autowired
    private TaskExecutor taskExecutor;

    public AsyncMailSender() {
    }

    public void send(SimpleMailMessage simpleMessage) throws MailException {
        taskExecutor.execute(new AsyncMailTask(simpleMessage));
    }

    public void send(SimpleMailMessage[] simpleMessages) throws MailException {
        for (SimpleMailMessage message : simpleMessages) {
            send(message);
        }
    }

    private class AsyncMailTask implements Runnable {

        private SimpleMailMessage message;

        private AsyncMailTask(SimpleMailMessage message) {
            this.message = message;
        }

        public void run() {
            mailSender.send(message);
        }
    }

}