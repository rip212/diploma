package com.rip21.diploma.service.helper;


import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * Created by alos on 27.05.2014.
 */
public class CellStyles {

    public static final String DATE_FORMAT = "d.m.YYyy hh:mm:ss";

    private CellStyles() {

    }

    /**
     * @param workbook pass workbook of the cell you work with
     * @return XSSFCellStyle to make cell date formatted.
     */
    public static XSSFCellStyle date(XSSFWorkbook workbook) {

        XSSFCellStyle style = workbook.createCellStyle();
        CreationHelper createHelper = workbook.getCreationHelper();
        style.setDataFormat(
                createHelper.createDataFormat().getFormat(DATE_FORMAT));

        return style;

    }

    /**
     * @param workbook pass workbook of the cell you work with
     * @return XSSFCellStyle to make cell green.
     */

    public static XSSFCellStyle greenBackground(XSSFWorkbook workbook) {
        XSSFCellStyle style = workbook.createCellStyle();
        style.setFillForegroundColor(HSSFColor.LIGHT_GREEN.index);
        style.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
        return style;
    }

}
