package com.rip21.diploma.service.helper;

import com.rip21.diploma.config.RootConfiguration;
import com.rip21.diploma.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Import;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;

/**
 * Created by puha2_000 on 14.05.14.
 */
@Service
@Import(RootConfiguration.class)
public class SendMailService {
    /*private static Logger log = Logger.getLogger(SendMailService.class);*/

    private
    @Value("${mail.sdeli.host}")
    String sdeliHost;
    private
    @Value("${mail.username}")
    String senderEmail;

    @Autowired
    private AsyncMailSender mailSender;


    public void sendEmail(String to, String subject, String body)
            throws MailException {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setFrom(senderEmail);
        message.setSubject(subject);
        message.setText(body);
        mailSender.send(message);
    }

    public void sendConfirmEmail(User sendTo) {
        String subject = "Your account has been registered";
        String body = "To place orders you must validate your account\n" +
                "to confirm your email address go to link below:\n" +
                "http://" + sdeliHost + "/emailConfirm?user=" + sendTo.getValidationCode();
        sendEmail(sendTo.getEmail(), subject, body);
    }

    public void sendForgotEmail(User sendTo) {
        String subject = "Password recovery";
        String body = "To change your password go to following link\n" +
                "http://" + sdeliHost + "/changePasswordForm?user=" + sendTo.getValidationCode();
        sendEmail(sendTo.getEmail(), subject, body);
    }

}