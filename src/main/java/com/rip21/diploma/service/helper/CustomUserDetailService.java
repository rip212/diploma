package com.rip21.diploma.service.helper;

import com.rip21.diploma.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Collections;

/**
 * Created by puha2_000 on 14.05.14.
 */
public class CustomUserDetailService implements UserDetailsService {

    @Autowired
    private UserService userService;

    public UserDetails loadUserByUsername(String username) {
        com.rip21.diploma.entity.User user = userService.getUserByLogin(username);
        if (user != null) {
            return new User(user.getLogin(), user.getPassword(), Collections.singleton(createAuthority(user)));
        } else {
            throw new UsernameNotFoundException("user not found");
        }
    }

    private GrantedAuthority createAuthority(com.rip21.diploma.entity.User user) {
        return new SimpleGrantedAuthority(user.getRole());
    }

}