package com.rip21.diploma.service;

import com.rip21.diploma.dao.AccountDao;
import com.rip21.diploma.entity.Account;
import com.rip21.diploma.exception.NotEnoughFundsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by puha2_000 on 24.05.14.
 */
@Service
public class AccountService {

    public static final String CHECKING = "Checking";
    public static final String SAVING = "Saving";
    @Autowired
    AccountDao accountDao;

    public List<Account> findAccountsByUserId(long id) {
        return accountDao.findByUser_Id(id);
    }

    public Account findAccountById(long id) {
        return accountDao.findOne(id);
    }

    public void deleteAccountById(long id) {
        accountDao.delete(id);
    }

    public BigDecimal deposit(Account account, BigDecimal bigDecimalAmount) {
        BigDecimal value = bigDecimalAmount;
        if (account.getType().equals(CHECKING)) {
            if (account.getOverdraft().compareTo(account.getOverdraftLimit()) == 0) {
                account.setBalance(account.getBalance().add(bigDecimalAmount));
            } else {
                account.setOverdraft(account.getOverdraft().add(bigDecimalAmount));
                if (account.getOverdraft().compareTo(account.getOverdraftLimit()) == 1) {
                    value = account.getOverdraft().subtract(account.getOverdraftLimit());
                    account.setOverdraft(account.getOverdraft().subtract(value));
                    account.setBalance(account.getBalance().add(value));
                }
            }
        } else {
            account.setBalance(account.getBalance().add(bigDecimalAmount));
        }
        accountDao.save(account);
        return bigDecimalAmount;
    }


    public BigDecimal withdraw(Account account, BigDecimal amountToWithdraw) throws NotEnoughFundsException {
        BigDecimal maxValueToWithdraw = (account.getBalance().add(account.getOverdraft()));
        BigDecimal creditFunds = account.getBalance().subtract(amountToWithdraw); // get if credit money will use
        if (creditFunds.compareTo(BigDecimal.ZERO) == -1) {
            creditFunds = creditFunds.abs();
            BigDecimal percent = (creditFunds.divide(new BigDecimal(100)).multiply(new BigDecimal(7.5))); //get percent for use credit money
            amountToWithdraw = amountToWithdraw.add(percent); // add them to whole withdraw amount
        }
        if (amountToWithdraw.compareTo(maxValueToWithdraw) == 1) { //amount > maxValueToWithdraw
            throw new NotEnoughFundsException();
        } else if (account.getType().equals(CHECKING)) {
            account.setBalance(account.getBalance().subtract(amountToWithdraw));
            if (account.getBalance().compareTo(BigDecimal.ZERO) == -1) { // balance < 0
                account.setOverdraft(account.getOverdraft().subtract(account.getBalance().abs()));
                account.setBalance(BigDecimal.ZERO);
            }
        } else {
            account.setBalance(account.getBalance().subtract(amountToWithdraw));
        }
        accountDao.save(account);
        return amountToWithdraw;
    }

    public void addAccount(Account newAccount) {
        accountDao.save(newAccount);
    }
}
