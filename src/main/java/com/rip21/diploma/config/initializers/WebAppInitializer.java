package com.rip21.diploma.config.initializers;

import com.rip21.diploma.config.MailConfig;
import com.rip21.diploma.config.PersistenceConfig;
import com.rip21.diploma.config.SecurityConfig;
import com.rip21.diploma.config.WebMvcConfig;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.Filter;

/**
 * Created by vx on 13.02.14.
 */
public class WebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{PersistenceConfig.class, SecurityConfig.class, MailConfig.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{WebMvcConfig.class};
    }

    @Override
    protected Filter[] getServletFilters() {
        CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
        characterEncodingFilter.setEncoding("UTF-8");
        characterEncodingFilter.setForceEncoding(false);
        return new Filter[]{characterEncodingFilter};
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }


}
