package com.rip21.diploma;

public final class RoleConstants {

    public static final String ROLE_ADMIN = "ADMIN";
    public static final String ROLE_USER = "USER";
    public static final String ROLE_INACTIVE_USER = "INACTIVE_USER";
    public static final String ROLE_ANONYMOUS = "ANONYMOUS";

    private RoleConstants() {

    }

}