package com.rip21.diploma.entity;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by puha2_000 on 06.05.14.
 */
@Entity
@Table(name = "history")

public class History implements Serializable {

    public History() {
    }

    public History(Account account, String type, String amount, String balance, String overdraft, Date date) {
        this.account = account;
        this.type = type;
        this.amount = amount;
        this.balance = balance;
        this.overdraft = overdraft;
        this.date = date;
    }

    @Id
    @GeneratedValue
    private long id;

    @ManyToOne
    @JoinColumn(name = "account_id", referencedColumnName = "id", nullable = false)
    private Account account;

    String type;
    String amount;
    String balance;
    String overdraft;
    Date date = new Date();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getOverdraft() {
        return overdraft;
    }

    public void setOverdraft(String overdraft) {
        this.overdraft = overdraft;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        History history = (History) o;

        if (id != history.id) return false;
        if (!amount.equals(history.amount)) return false;
        if (!balance.equals(history.balance)) return false;
        if (!date.equals(history.date)) return false;
        if (!overdraft.equals(history.overdraft)) return false;
        if (!type.equals(history.type)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + type.hashCode();
        result = 31 * result + amount.hashCode();
        result = 31 * result + balance.hashCode();
        result = 31 * result + overdraft.hashCode();
        result = 31 * result + date.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "History{" +
                "id=" + id +
                ", account=" + account +
                ", type='" + type + '\'' +
                ", amount='" + amount + '\'' +
                ", balance='" + balance + '\'' +
                ", overdraft='" + overdraft + '\'' +
                ", date=" + date +
                '}';
    }
}
