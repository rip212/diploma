package com.rip21.diploma.controller;

import com.rip21.diploma.RoleConstants;
import com.rip21.diploma.dto.UserIdNewRoleDto;
import com.rip21.diploma.entity.User;
import com.rip21.diploma.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
public class AdminController {


    @Autowired
    private UserService userService;

    @ModelAttribute
    public UserIdNewRoleDto userIdNewRoleDto() {
        return new UserIdNewRoleDto();
    }

    @RequestMapping(value = "/admin/managing", method = RequestMethod.GET)
    public String manageUsers(ModelMap model) {
        List<User> users = userService.getAllUsers();
        List<String> roles = new ArrayList<String>() {
            {
                add(RoleConstants.ROLE_ADMIN);
                add(RoleConstants.ROLE_USER);
                add(RoleConstants.ROLE_INACTIVE_USER);
            }
        };


        model.addAttribute("users", users); //usersDto
        model.addAttribute("roles", roles);
        return "/adminManagement";
    }

    @RequestMapping(value = {"/admin/managing/delete"}, method = RequestMethod.GET)
    public String deleteUser(@RequestParam("id") String id) {
        userService.deleteUserById(Long.parseLong(id));
        return "redirect:/admin/managing";
    }

    @RequestMapping(value = "/admin/managing", method = RequestMethod.POST)
    public String modifyUserRole(@Valid UserIdNewRoleDto userIdNewRoleDto, BindingResult bindingResult, ModelMap modelMap) {

        if (bindingResult.hasErrors()) {
            System.out.println("Validation error!");
            return "redirect:/admin/managing";
        }

        User user = userService.getUserById(userIdNewRoleDto.getId());
        user.setRole(userIdNewRoleDto.getNewRole());

        userService.saveUser(user);
        return "redirect:/admin/managing";
    }
}
