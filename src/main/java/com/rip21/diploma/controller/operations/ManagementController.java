package com.rip21.diploma.controller.operations;

import com.rip21.diploma.entity.Account;
import com.rip21.diploma.entity.User;
import com.rip21.diploma.service.AccountService;
import com.rip21.diploma.service.HistoryService;
import com.rip21.diploma.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by puha2_000 on 25.05.14.
 */
@Controller
public class ManagementController {

    @Autowired
    AccountService accountService;

    @Autowired
    HistoryService historyService;

    @Autowired
    UserService userService;
    Long activeAccountId = 0L;

    @RequestMapping(value = "/management", method = RequestMethod.GET)
    public String manageAccounts(ModelMap model) {
        activeAccountId = 0L;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User activeUser = userService.getUserByLogin(auth.getName());
        List<Account> userAccounts = accountService.findAccountsByUserId(activeUser.getId());

        model.addAttribute("accounts", userAccounts);

        return "/operations/management";
    }

    @RequestMapping(value = "/managementOperation", method = RequestMethod.GET)
    public String operation(ModelMap model, @ModelAttribute("accountId") long accountId) {
        activeAccountId = accountId;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User activeUser = userService.getUserByLogin(auth.getName());
        try {
            Account activeAccount = accountService.findAccountById(activeAccountId);

            if (activeAccount != null && activeAccount.getUser().getId() == activeUser.getId()) {
                if (activeAccount.getBalance().toString().equals("0.00")) {
                    historyService.deleteAllHistory(activeAccount);
                    accountService.deleteAccountById(activeAccount.getId());
                    model.addAttribute("message", "success");
                    List<Account> userAccounts = accountService.findAccountsByUserId(activeUser.getId());
                    model.addAttribute("accounts", userAccounts);
                    return "/operations/management";
                } else {
                    model.addAttribute("message", "error");
                }
            }
        } catch (Exception e) {
        } finally {
            List<Account> userAccounts = accountService.findAccountsByUserId(activeUser.getId());
            model.addAttribute("accounts", userAccounts);
        }

        return "/operations/management";
    }


    @RequestMapping(value = "/addAccount", method = RequestMethod.POST)
    public String add(ModelMap model, @ModelAttribute("type") String type) {
        activeAccountId = 0L;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User activeUser = userService.getUserByLogin(auth.getName());

        Account newAccount = new Account();
        newAccount.setType(type);
        newAccount.setUser(activeUser);
        if (type.equals("Checking")) {
            newAccount.setOverdraftLimit(BigDecimal.valueOf(1000));
            newAccount.setOverdraft(BigDecimal.valueOf(1000));
        }
        accountService.addAccount(newAccount);
        List<Account> userAccounts = accountService.findAccountsByUserId(activeUser.getId());
        model.addAttribute("accounts", userAccounts);
        model.addAttribute("message", "added");
        return "/operations/management";
    }

}
