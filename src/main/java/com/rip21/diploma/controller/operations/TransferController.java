package com.rip21.diploma.controller.operations;

import com.rip21.diploma.entity.Account;
import com.rip21.diploma.entity.User;
import com.rip21.diploma.exception.NotEnoughFundsException;
import com.rip21.diploma.exception.SameAccountsException;
import com.rip21.diploma.service.AccountService;
import com.rip21.diploma.service.HistoryService;
import com.rip21.diploma.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.math.BigDecimal;
import java.util.List;

@Controller
public class TransferController {


    @Autowired
    UserService userService;

    @Autowired
    AccountService accountService;
    Long activeAccountId;
    Long activeTargetAccountId;

    @Autowired
    HistoryService historyService;

    @RequestMapping(value = "/transfer", method = RequestMethod.GET)
    public String manageUsers(ModelMap model) {
        activeAccountId = null;
        activeTargetAccountId = null;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User activeUser = userService.getUserByLogin(auth.getName());
        List<Account> userAccounts = accountService.findAccountsByUserId(activeUser.getId());

        model.addAttribute("accounts", userAccounts);

        return "/operations/transfer";
    }

    @RequestMapping(value = "/transferOperation", method = RequestMethod.GET)
    public String operation(ModelMap model, @ModelAttribute("accountId") long accountId) {
        activeAccountId = accountId;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User activeUser = userService.getUserByLogin(auth.getName());
        Account activeAccount = accountService.findAccountById(activeAccountId);
        if (activeAccount != null && activeAccount.getUser().getId() == activeUser.getId()) {
            model.addAttribute("account", activeAccount);
            return "/operations/transferOperation";
        } else {
            activeAccountId = null;
            List<Account> userAccounts = accountService.findAccountsByUserId(activeUser.getId());
            model.addAttribute("accounts", userAccounts);
            return "/operations/transfer";
        }
    }

    @RequestMapping(value = "/transferOperation", method = RequestMethod.POST)
    public String transfer(ModelMap model, @ModelAttribute("amount") String amount,
                           @ModelAttribute("targetAccountId") String targetAccountId) {
        Account activeAccount = accountService.findAccountById(activeAccountId);

        try {
            activeTargetAccountId = Long.valueOf(targetAccountId);
            Account targetAccount = accountService.findAccountById(activeTargetAccountId);
            if (activeAccount.getId() == targetAccount.getId()) {
                throw new SameAccountsException();
            }
            if (Double.valueOf(amount) <= 0) {
                throw new Exception();
            }
            BigDecimal bigDecimalAmount = BigDecimal.valueOf(Double.valueOf(amount));

            model.addAttribute("targetAccountId", targetAccountId);
            model.addAttribute("targetUser", targetAccount.getUser());
            model.addAttribute("amount", bigDecimalAmount);
            model.addAttribute("message", "success");
        } catch (NumberFormatException e) {
            model.addAttribute("message", "stringError");
            return "/operations/transferOperation";
        } catch (SameAccountsException e) {
            model.addAttribute("message", "sameAccountError");
            return "/operations/transferOperation";
        } catch (NullPointerException e) {
            model.addAttribute("message", "accountNotFoundError");
            return "/operations/transferOperation";
        } catch (Exception e) {
            model.addAttribute("message", "lessThenZeroError");
            return "/operations/transferOperation";
        } finally {
            model.addAttribute("accountId", activeAccountId);
            model.addAttribute("account", activeAccount);
        }
        return "/operations/transferOperationProceed";
    }

    @RequestMapping(value = "/transferOperationProceed", method = RequestMethod.POST)
    public String transferProceed(ModelMap model, @ModelAttribute("amount") String amount,
                                  @ModelAttribute("targetAccountId") String targetAccountId) {
        activeTargetAccountId = Long.valueOf(targetAccountId);
        Account activeAccount = accountService.findAccountById(activeAccountId);
        Account targetAccount = accountService.findAccountById(activeTargetAccountId);
        BigDecimal bigDecimalAmount = BigDecimal.valueOf(Double.valueOf(amount));
        try {
            BigDecimal withdrawAmount = accountService.withdraw(activeAccount, bigDecimalAmount);
            historyService.addHistory(activeAccount, withdrawAmount, HistoryService.TRANSFER);
            BigDecimal depositAmount = accountService.deposit(targetAccount, bigDecimalAmount);
            historyService.addHistory(targetAccount, depositAmount, HistoryService.TRANSFER);
            model.addAttribute("message", "success");
        } catch (NotEnoughFundsException e) {
            model.addAttribute("message", "notEnoughFundsError");
        } finally {
            model.addAttribute("accountId", activeAccountId);
            model.addAttribute("account", activeAccount);
        }
        return "/operations/transferOperation";
    }

}
