package com.rip21.diploma.controller.operations;

import com.rip21.diploma.entity.Account;
import com.rip21.diploma.entity.User;
import com.rip21.diploma.exception.NotEnoughFundsException;
import com.rip21.diploma.service.AccountService;
import com.rip21.diploma.service.HistoryService;
import com.rip21.diploma.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.math.BigDecimal;
import java.util.List;

@Controller
public class WithdrawController {


    @Autowired
    UserService userService;

    @Autowired
    AccountService accountService;
    Long activeAccountId;

    @Autowired
    HistoryService historyService;

    @RequestMapping(value = "/withdraw", method = RequestMethod.GET)
    public String showWithdraw(ModelMap model) {
        activeAccountId = null;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User activeUser = userService.getUserByLogin(auth.getName());
        List<Account> userAccounts = accountService.findAccountsByUserId(activeUser.getId());
        model.addAttribute("accounts", userAccounts);
        return "/operations/withdraw";
    }

    @RequestMapping(value = "/withdrawOperation", method = RequestMethod.GET)
    public String operation(ModelMap model, @ModelAttribute("accountId") long accountId) {
        activeAccountId = accountId;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User activeUser = userService.getUserByLogin(auth.getName());
        Account activeAccount = accountService.findAccountById(activeAccountId);
        if (activeAccount != null && activeAccount.getUser().getId() == activeUser.getId()) {
            model.addAttribute("account", activeAccount);
            return "/operations/withdrawOperation";
        } else {
            activeAccountId = null;
            List<Account> userAccounts = accountService.findAccountsByUserId(activeUser.getId());
            model.addAttribute("accounts", userAccounts);
            return "/operations/withdraw";
        }
    }

    @RequestMapping(value = "/withdrawOperation", method = RequestMethod.POST)
    public String deposit(ModelMap model, @ModelAttribute("amount") String amount) {
        Account activeAccount = accountService.findAccountById(activeAccountId);
        try {
            if (Double.valueOf(amount) <= 0) {
                throw new Exception();
            }
            BigDecimal bigDecimalAmount = BigDecimal.valueOf(Double.valueOf(amount));
            BigDecimal withdrawAmount = accountService.withdraw(activeAccount, bigDecimalAmount);
            historyService.addHistory(activeAccount, withdrawAmount, HistoryService.WITHDRAW);
            model.addAttribute("message", "success");
        } catch (NumberFormatException e) {
            model.addAttribute("message", "stringError");
        } catch (NotEnoughFundsException e) {
            model.addAttribute("message", "notEnoughFundsError");
        } catch (Exception e) {
            model.addAttribute("message", "lessThenZeroError");
        } finally {
            model.addAttribute("accountId", activeAccountId);
            model.addAttribute("account", activeAccount);
        }
        return "/operations/withdrawOperation";
    }


}
