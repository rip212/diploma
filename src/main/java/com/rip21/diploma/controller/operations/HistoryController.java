package com.rip21.diploma.controller.operations;

import com.rip21.diploma.entity.Account;
import com.rip21.diploma.entity.History;
import com.rip21.diploma.entity.User;
import com.rip21.diploma.service.AccountService;
import com.rip21.diploma.service.HistoryService;
import com.rip21.diploma.service.UserService;
import com.rip21.diploma.service.helper.ExcelGenerator;
import org.apache.commons.io.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;

@Controller
public class HistoryController {

    @Autowired
    UserService userService;

    @Autowired
    AccountService accountService;
    Long activeAccountId;

    @Autowired
    HistoryService historyService;

    @RequestMapping(value = "/history", method = RequestMethod.GET)
    public String showWithdraw(ModelMap model) {
        activeAccountId = null;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User activeUser = userService.getUserByLogin(auth.getName());
        List<Account> userAccounts = accountService.findAccountsByUserId(activeUser.getId());

        model.addAttribute("accounts", userAccounts);

        return "/operations/history";
    }

    @RequestMapping(value = "/historyOperation", method = RequestMethod.GET)
    public String withdraw(ModelMap model, @ModelAttribute("accountId") long accountId) {
        activeAccountId = accountId;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User activeUser = userService.getUserByLogin(auth.getName());
        Account activeAccount = accountService.findAccountById(activeAccountId);
        if (activeAccount != null && activeAccount.getUser().getId() == activeUser.getId()) {
            model.addAttribute("account", activeAccount);
            List<History> accountHistory = historyService.findHistoryByAccountId(activeAccount.getId());
            model.addAttribute("accountHistory", accountHistory);
            return "/operations/historyOperation";
        } else {
            activeAccountId = null;
            List<Account> userAccounts = accountService.findAccountsByUserId(activeUser.getId());
            model.addAttribute("accounts", userAccounts);
            return "/operations/history";
        }
    }

    @RequestMapping(value = "/getReport", method = RequestMethod.GET)
    public void getFile(HttpServletResponse response) {

        List<History> historyList = historyService.findHistoryByAccountId(activeAccountId);
        String fileName = "D:\\report.xlsx";
        try {
            XSSFWorkbook workbook = ExcelGenerator.getWorkbook(historyList);
            FileOutputStream out = new FileOutputStream(new File(fileName));
            workbook.write(out);
            out.close();


            File fileToDownload = new File(fileName);
            InputStream inputStream = new FileInputStream(fileToDownload);
            response.setContentType("application/force-download");
            response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
            IOUtils.copy(inputStream, response.getOutputStream());
            response.flushBuffer();
        } catch (IOException ex) {

            throw new RuntimeException("IOError writing file to output stream");
        }

    }

}
