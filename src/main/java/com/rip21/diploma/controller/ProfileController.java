package com.rip21.diploma.controller;

import com.rip21.diploma.entity.User;
import com.rip21.diploma.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;

/**
 * Created by puha2_000 on 15.05.14.
 */
@Controller
public class ProfileController {

    @Autowired
    private UserService userService;


    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public String printUserProfile(ModelMap modelMap, Principal principal) {
        if (principal == null) {
            return "login";
        }
        String userlogin = principal.getName();
        User currentUser = userService.getUserByLogin(userlogin);
        currentUser.getName();
        currentUser.getLogin();
        currentUser.getEmail();
        currentUser.getPhone();

        modelMap.addAttribute("user", currentUser);

        return "profile";
    }
}