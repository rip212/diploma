package com.rip21.diploma.controller;

import com.rip21.diploma.RoleConstants;
import com.rip21.diploma.entity.User;
import com.rip21.diploma.service.UserService;
import com.rip21.diploma.service.helper.CustomUserDetailService;
import com.rip21.diploma.service.helper.RegistrationData;
import com.rip21.diploma.service.helper.RegistrationValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.validation.Valid;
import java.util.Map;

/**
 * Created by puha2_000 on 13.05.14.
 */

@Controller
public class RegistrationController {
    @Autowired
    private UserService userService;
    @Autowired
    private RegistrationValidator registrationValidator;
    @Autowired
    private CustomUserDetailService userDetailService;
    @Autowired
    private BCryptPasswordEncoder encoder;

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registration(Map model) {
        return "/registration/registration";
    }

    // @ModelAttribute("registrationData") tries to create registrationData from variables of POST form in html page
    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String processAddingUser(Map model, @ModelAttribute("registrationData") @Valid RegistrationData data, BindingResult errors) {
        ValidationUtils.invokeValidator(registrationValidator, data, errors);
        if (errors.hasErrors()) {
            model.put("data", data);
            model.put("errors", errors.getAllErrors());
            return "/registration/registration";
        }
        String psw = encoder.encode(data.getPassword());
        data.setPassword(psw);
        userService.registerUser(data);
        return "/registration/registrationSuccess";
    }

    @RequestMapping(value = "/registrationSuccess", method = RequestMethod.GET)
    public String registrationSuccess(Map model) {
        return "/registration/registrationSuccess";
    }

    @RequestMapping(value = "/emailConfirm", method = RequestMethod.GET)
    public String confirmEmail(Map model, @RequestParam("user") String userHash) {
        User user = userService.getUserByValidationCode(Integer.valueOf(userHash));
        if (user != null && user.getRole().equals(RoleConstants.ROLE_INACTIVE_USER)) {
            user.setRole(RoleConstants.ROLE_USER);
            userService.saveUser(user);
            Authentication authentication = new UsernamePasswordAuthenticationToken(
                    userDetailService.loadUserByUsername(user.getLogin()),
                    null,
                    AuthorityUtils.createAuthorityList(user.getRole()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return "/registration/emailConfirm";
        }
        return "redirect:/";
    }
}