/*USERS*/
INSERT INTO public.users (id, email, login, name, password, phone, role, validationcode)
VALUES (1, 'puha212@gmail.com', 'admin', 'name', '$2a$10$iD63Lpe8HDTb15yk75xtIO0efij/z63FmZpwnV09QiuaMB0cskFdu', NULL,
        'ADMIN', -1055193758);
INSERT INTO public.users (id, email, login, name, password, phone, role, validationcode)
VALUES
  (2, 'antiemorap@mail.ru', 'user', 'username', '$2a$10$1b7nFaUBYgtLkuRPBjPhCO3EEppyGZCMzFkpDuIlAMNDb/UQMxZ26', NULL,
   'USER', -1381272991);

CREATE TABLE banks
(
  id   BIGINT PRIMARY KEY NOT NULL,
  name VARCHAR(255)
);

CREATE TABLE accounts
(
  id              BIGINT PRIMARY KEY NOT NULL,
  balance         NUMERIC(19),
  overdraft       NUMERIC(19),
  overdraft_limit NUMERIC(19),
  type            VARCHAR(255),
  user_id         BIGINT             NOT NULL
);
CREATE TABLE history
(
  id         BIGINT PRIMARY KEY NOT NULL,
  amount     VARCHAR(255),
  balance    VARCHAR(255),
  date       TIMESTAMP,
  overdraft  VARCHAR(255),
  type       VARCHAR(255),
  account_id BIGINT             NOT NULL
);
CREATE TABLE users
(
  id             BIGINT PRIMARY KEY NOT NULL,
  email          VARCHAR(255),
  login          VARCHAR(255),
  name           VARCHAR(255),
  password       VARCHAR(255),
  phone          VARCHAR(255),
  role           VARCHAR(255),
  validationcode INT                NOT NULL,
  bank_id        BIGINT             NOT NULL
);
ALTER TABLE accounts ADD FOREIGN KEY (user_id) REFERENCES users (id);
ALTER TABLE history ADD FOREIGN KEY (account_id) REFERENCES accounts (id);
ALTER TABLE users ADD FOREIGN KEY (bank_id) REFERENCES banks (id);

